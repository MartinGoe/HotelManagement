package de.cube.hotel.gui.scenes;

import de.cube.hotel.employees.classes.Chef;
import de.cube.hotel.employees.classes.Manager;
import de.cube.hotel.employees.classes.Washer;
import de.cube.hotel.employees.functions.ManageEmployees;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.util.converter.IntegerStringConverter;

import javax.swing.*;

public class Employees {
    public ManageEmployees manageEmployees = new ManageEmployees();


    public TabPane create() {
        TabPane tabPane = new TabPane();

        Tab chefTab = new Tab("Chefs");
        BorderPane chefPane = new BorderPane();
        Button new_chef = new Button("New Chef");
        new_chef.setOnAction(event -> {
            String firstName = JOptionPane.showInputDialog("What is the First name?");
            String lastName = JOptionPane.showInputDialog("What is the Last name?");
            int age = Integer.parseInt(JOptionPane.showInputDialog("What is his/her age?"));
            String email = JOptionPane.showInputDialog("What is his/her email address?");

            manageEmployees.addChef(age, firstName, lastName, email);
            chefPane.setCenter(chefTableView());
        });
        Button remove_chef = new Button("Remove Chef");
        remove_chef.setOnAction(event -> {
            manageEmployees.removeChef(Integer.parseInt(JOptionPane.showInputDialog("What is the id?")));
            chefPane.setCenter(chefTableView());
        });
        HBox chefBox = new HBox();
        chefBox.getChildren().addAll(new_chef, remove_chef);
        chefPane.setTop(chefBox);
        chefPane.setCenter(chefTableView());
        chefTab.setContent(chefPane);
        tabPane.getTabs().add(chefTab);

        Tab managerTab = new Tab("Managers");
        BorderPane managerPane = new BorderPane();

        Button new_manager = new Button("New Manager");
        new_manager.setOnAction(event -> {
            String firstName = JOptionPane.showInputDialog("What is the First name?");
            String lastName = JOptionPane.showInputDialog("What is the Last name?");
            int age = Integer.parseInt(JOptionPane.showInputDialog("What is his/her age?"));
            String email = JOptionPane.showInputDialog("What is his/her email address?");

            manageEmployees.addManager(age, firstName, lastName, email);
            managerPane.setCenter(managerTableView());
        });
        Button remove_manager = new Button("Remove Manager");
        remove_manager.setOnAction(event -> {
            manageEmployees.removeManager(Integer.parseInt(JOptionPane.showInputDialog("What is the id?")));
            managerPane.setCenter(managerTableView());
        });
        HBox managerBox = new HBox();
        managerBox.getChildren().addAll(new_manager, remove_manager);
        managerPane.setTop(managerBox);
        managerPane.setCenter(managerTableView());
        managerTab.setContent(managerPane);
        tabPane.getTabs().add(managerTab);

        Tab washerTab = new Tab("Washers");
        BorderPane washerPane = new BorderPane();

        Button new_washer = new Button("New Washer");
        new_washer.setOnAction(event -> {
            String firstName = JOptionPane.showInputDialog("What is the First name?");
            String lastName = JOptionPane.showInputDialog("What is the Last name?");
            int age = Integer.parseInt(JOptionPane.showInputDialog("What is his/her age?"));
            String email = JOptionPane.showInputDialog("What is his/her email address?");

            manageEmployees.addWasher(age, firstName, lastName, email);
            managerPane.setCenter(washerTableView());
        });
        Button remove_washer = new Button("Remove Washer");
        remove_washer.setOnAction(event -> {
            manageEmployees.removeWasher(Integer.parseInt(JOptionPane.showInputDialog("What is the id?")));
            washerPane.setCenter(washerTableView());
        });
        HBox washerBox = new HBox();
        washerBox.getChildren().addAll(new_washer, remove_washer);
        washerPane.setTop(washerBox);
        washerPane.setCenter(washerTableView());
        washerTab.setContent(washerPane);
        tabPane.getTabs().add(washerTab);

        return tabPane;
    }

    private TableView chefTableView() {
        TableView<Chef> customerTableView = new TableView<>();
        customerTableView.setEditable(true);


        TableColumn<Chef, Integer> id = new TableColumn<>("id");
        id.setEditable(false);

        TableColumn<Chef, String> fullName = new TableColumn<>("Full Name");

        TableColumn<Chef, String> firstName = new TableColumn<>("First Name");
        firstName.setCellFactory(TextFieldTableCell.forTableColumn());
        firstName.setOnEditCommit(event -> {
            Chef rowValue = event.getRowValue();
            rowValue.setFirstName(event.getNewValue());
        });


        TableColumn<Chef, String> lastName = new TableColumn<>("Last Name");
        lastName.setCellFactory(TextFieldTableCell.forTableColumn());
        lastName.setOnEditCommit(event -> event.getRowValue().setLastName(event.getNewValue()));


        TableColumn<Chef, Integer> age = new TableColumn<>("Age");
        age.setCellFactory(TextFieldTableCell.forTableColumn(new IntegerStringConverter()));
        age.setOnEditCommit(event -> event.getRowValue().setAge(event.getNewValue()));

        TableColumn<Chef, String> email = new TableColumn<>("Email");
        email.setCellFactory(TextFieldTableCell.forTableColumn());
        email.setOnEditCommit(event -> event.getRowValue().setEmail(event.getNewValue()));


        firstName.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        lastName.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        age.setCellValueFactory(new PropertyValueFactory<>("age"));
        email.setCellValueFactory(new PropertyValueFactory<>("email"));
        id.setCellValueFactory(new PropertyValueFactory<>("id"));

        fullName.getColumns().addAll(firstName, lastName);

        ObservableList<Chef> list = FXCollections.observableArrayList();
        list.addAll(manageEmployees.getChefs());

        customerTableView.getColumns().addAll(id, fullName, age, email);
        customerTableView.setItems(list);
        return customerTableView;
    }

    private TableView managerTableView() {
        TableView<Manager> customerTableView = new TableView<>();

        TableColumn<Manager, Integer> id = new TableColumn<>("id");
        id.setEditable(false);

        TableColumn<Manager, String> fullName = new TableColumn<>("Full Name");

        TableColumn<Manager, String> firstName = new TableColumn<>("First Name");
        firstName.setCellFactory(TextFieldTableCell.forTableColumn());

        firstName.setOnEditCommit(event -> event.getRowValue().setFirstName(event.getNewValue()));

        TableColumn<Manager, String> lastName = new TableColumn<>("Last Name");
        lastName.setCellFactory(TextFieldTableCell.forTableColumn());
        lastName.setOnEditCommit(event -> event.getRowValue().setLastName(event.getNewValue()));

        TableColumn<Manager, Integer> age = new TableColumn<>("Age");
        age.setCellFactory(TextFieldTableCell.forTableColumn(new IntegerStringConverter()));
        age.setOnEditCommit(event -> event.getRowValue().setAge(event.getNewValue()));

        TableColumn<Manager, String> email = new TableColumn<>("Email");
        email.setCellFactory(TextFieldTableCell.forTableColumn());
        email.setOnEditCommit(event -> event.getRowValue().setEmail(event.getNewValue()));

        firstName.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        lastName.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        age.setCellValueFactory(new PropertyValueFactory<>("age"));
        email.setCellValueFactory(new PropertyValueFactory<>("email"));
        id.setCellValueFactory(new PropertyValueFactory<>("id"));

        fullName.getColumns().addAll(firstName, lastName);

        ObservableList<Manager> list = FXCollections.observableArrayList();
        list.addAll(manageEmployees.getManagers());

        customerTableView.getColumns().addAll(id, fullName, age, email);
        customerTableView.setItems(list);
        return customerTableView;
    }

    private TableView washerTableView() {
        TableView<Washer> customerTableView = new TableView<>();

        TableColumn<Washer, Integer> id = new TableColumn<>("id");
        id.setEditable(false);

        TableColumn<Washer, String> fullName = new TableColumn<>("Full Name");

        TableColumn<Washer, String> firstName = new TableColumn<>("First Name");
        firstName.setCellFactory(TextFieldTableCell.forTableColumn());
        firstName.setOnEditCommit(event -> event.getRowValue().setFirstName(event.getNewValue()));

        TableColumn<Washer, String> lastName = new TableColumn<>("Last Name");
        lastName.setCellFactory(TextFieldTableCell.forTableColumn());
        lastName.setOnEditCommit(event -> event.getRowValue().setLastName(event.getNewValue()));

        TableColumn<Washer, Integer> age = new TableColumn<>("Age");
        age.setCellFactory(TextFieldTableCell.forTableColumn(new IntegerStringConverter()));
        age.setOnEditCommit(event -> event.getRowValue().setAge(event.getNewValue()));

        TableColumn<Washer, String> email = new TableColumn<>("Email");
        email.setCellFactory(TextFieldTableCell.forTableColumn());
        email.setOnEditCommit(event -> event.getRowValue().setEmail(event.getNewValue()));

        firstName.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        lastName.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        age.setCellValueFactory(new PropertyValueFactory<>("age"));
        email.setCellValueFactory(new PropertyValueFactory<>("email"));
        id.setCellValueFactory(new PropertyValueFactory<>("id"));

        fullName.getColumns().addAll(firstName, lastName);

        ObservableList<Washer> list = FXCollections.observableArrayList();
        list.addAll(manageEmployees.getWashers());

        customerTableView.getColumns().addAll(id, fullName, age, email);
        customerTableView.setItems(list);
        return customerTableView;
    }
}
