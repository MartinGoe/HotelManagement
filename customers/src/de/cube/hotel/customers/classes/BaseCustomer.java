package de.cube.hotel.customers.classes;

public class BaseCustomer {

    private int id;

    private int age;

    public int getRoomId() {
        return roomId;
    }

    public void setRoomId(int roomId) {
        this.roomId = roomId;
    }

    private int roomId;

    private String firstName;
    private String lastName;

    private String email;

    private boolean hasChanged = false;

    public BaseCustomer(int id, int age, String firstName, String lastName, String email, int roomId) {
        this.id = id;
        this.age = age;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.roomId = roomId;
    }

    public int getId() {
        return id;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        hasChanged = true;
        this.age = age;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        hasChanged = true;
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        hasChanged = true;
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        hasChanged = true;
        this.email = email;
    }

    public boolean hasChanged (){return hasChanged;};
}
