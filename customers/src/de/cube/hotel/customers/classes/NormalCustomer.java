package de.cube.hotel.customers.classes;

public class NormalCustomer extends BaseCustomer {
    public NormalCustomer(int id, int age, String firstName, String lastName, String email, int roomId) {
        super(id, age, firstName, lastName, email, roomId);
    }
}
