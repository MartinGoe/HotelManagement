package de.cube.hotel.customers.functions;

import de.cube.hotel.customers.classes.NormalCustomer;
import de.cube.hotel.customers.classes.VIP;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Utilities {
    static int getRowOneColumnOne(ResultSet rs){
        try {
            rs.next();
            return rs.getInt(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static void updateNormalCustomer(NormalCustomer normalCustomer, Connection connection) {
        try {
            PreparedStatement statement = connection.prepareStatement("UPDATE normalcustomer SET age=?, firstName=?, lastName=?, email=? WHERE id=?");
            statement.setInt(1, normalCustomer.getAge());
            statement.setString(2, normalCustomer.getFirstName());
            statement.setString(3, normalCustomer.getLastName());
            statement.setString(4, normalCustomer.getEmail());

            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateVIP(VIP vip, Connection connection) {
        try {
            PreparedStatement statement = connection.prepareStatement("UPDATE vipcustomers SET age=?, firstName=?, lastName=?, email=? WHERE id=?");
            statement.setInt(1, vip.getAge());
            statement.setString(2, vip.getFirstName());
            statement.setString(3, vip.getLastName());
            statement.setString(4, vip.getEmail());

            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
