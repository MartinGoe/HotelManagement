package de.cube.hotel.customers.functions;

import de.cube.hotel.customers.classes.BaseCustomer;
import de.cube.hotel.customers.classes.NormalCustomer;
import de.cube.hotel.customers.classes.VIP;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.*;
import java.util.ArrayList;

public class ManageCustumers {
    Connection connection;

    public ArrayList<NormalCustomer> getNormalCustomers() {
        return normalCustomers;
    }

    public ArrayList<VIP> getVipCustomers() {
        return vipCustomers;
    }

    ArrayList<NormalCustomer> normalCustomers = new ArrayList<>();
    ArrayList<VIP> vipCustomers = new ArrayList<>();

    public ManageCustumers() {
        try {
            String[] info = Files.readAllLines(Paths.get("customers\\res\\password.txt")).toArray(new String[0]);
            connection = DriverManager.getConnection("jdbc:mysql://" + info[0] + ":3306/HotelManagement", info[1], info[2]);

            PreparedStatement normalStatement = connection.prepareStatement("SELECT * from normalcustomer");
            ResultSet normalSet = normalStatement.executeQuery();
            while(normalSet.next()){
                normalCustomers.add(new NormalCustomer(normalSet.getInt(1), normalSet.getInt(2), normalSet.getString(3)
                        , normalSet.getString(4), normalSet.getString(5), normalSet.getInt(6)));
            }

            PreparedStatement vipStatement = connection.prepareStatement("SELECT * from vipcustomer");
            ResultSet vipSet = vipStatement.executeQuery();
            while(vipSet.next()){
                vipCustomers.add(new VIP(vipSet.getInt(1), vipSet.getInt(2), vipSet.getString(3)
                        , vipSet.getString(4), vipSet.getString(5), vipSet.getInt(6)));
            }

        } catch (IOException | SQLException e) {
            e.printStackTrace();
        }
    }

    public NormalCustomer addNomalCustomer(int age, String firstName, String lastName, String email, int roomId){
        try {
            PreparedStatement statement = connection.prepareStatement("INSERT INTO normalcustomer VALUES (DEFAULT, ?, ?, ?, ?, ?);", Statement.RETURN_GENERATED_KEYS);
            statement.setInt(1, age);
            statement.setString(2, firstName);
            statement.setString(3, lastName);
            statement.setString(4, email);
            statement.setInt(5, roomId);
            statement.executeUpdate();

            int id = Utilities.getRowOneColumnOne(statement.getGeneratedKeys());

            NormalCustomer normalCustomer = new NormalCustomer(id, age, firstName, lastName, email, roomId);

            normalCustomers.add(normalCustomer);

            return normalCustomer;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public VIP addVIP(int age, String firstName, String lastName, String email, int roomId){
        try {
            PreparedStatement statement = connection.prepareStatement("INSERT INTO vipcustomers VALUES (DEFAULT, ?, ?, ?, ?, ?);", Statement.RETURN_GENERATED_KEYS);
            statement.setInt(1, age);
            statement.setString(2, firstName);
            statement.setString(3, lastName);
            statement.setString(4, email);
            statement.setInt(5, roomId);
            statement.executeUpdate();

            int id = Utilities.getRowOneColumnOne(statement.getGeneratedKeys());

            VIP vip = new VIP(id, age, firstName, lastName, email, roomId);

            vipCustomers.add(vip);

            return vip;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void removeNormalCustomer(int id){
        try {
            PreparedStatement statement = connection.prepareStatement("DELETE FROM normalcustomer WHERE id=?");
            statement.setInt(1, id);
            statement.executeUpdate();

            normalCustomers.removeIf(normalCustomer -> normalCustomer.getId() == id);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void removeVIP(int id){
        try {
            PreparedStatement statement = connection.prepareStatement("DELETE FROM vipcustomers WHERE id=?");
            statement.setInt(1, id);
            statement.executeUpdate();

            vipCustomers.removeIf(vip -> vip.getId() == id);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public NormalCustomer getNormalCustomerById(int id){
        return normalCustomers.stream().filter(normalCustomer -> normalCustomer.getId() == id).findFirst().orElse(new NormalCustomer(0, 0, null, null, null, 0));
    }

    public VIP getVIPById(int id){
        return vipCustomers.stream().filter(vip -> vip.getId() == id).findFirst().orElse(new VIP(0, 0, null, null, null, 0));
    }

    public void save(){
        normalCustomers.stream().filter(BaseCustomer::hasChanged).forEach(normalCustomer -> Utilities.updateNormalCustomer(normalCustomer, connection));
        vipCustomers.stream().filter(BaseCustomer::hasChanged).forEach(vip -> Utilities.updateVIP(vip, connection));
    }


}
