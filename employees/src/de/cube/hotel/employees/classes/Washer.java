package de.cube.hotel.employees.classes;

public class Washer extends BaseEmployee {
    public Washer(int id, int age, String firstName, String lastName, String email) {
        super(id, age, firstName, lastName, email, 8.5);
    }
}
