package de.cube.hotel.employees.classes;

public class Manager  extends BaseEmployee{
    public Manager(int id, int age, String firstName, String lastName, String email) {
        super(id, age, firstName, lastName, email, 15);
    }
}
