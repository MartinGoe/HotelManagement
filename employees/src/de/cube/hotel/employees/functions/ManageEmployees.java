package de.cube.hotel.employees.functions;

import de.cube.hotel.employees.classes.BaseEmployee;
import de.cube.hotel.employees.classes.Chef;
import de.cube.hotel.employees.classes.Manager;
import de.cube.hotel.employees.classes.Washer;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.*;
import java.util.ArrayList;

public class ManageEmployees {

    private Connection connection;


    private ArrayList<Chef> chefs = new ArrayList<>();



    private ArrayList<Manager> managers = new ArrayList<>();
    private ArrayList<Washer> washers = new ArrayList<>();

    public ManageEmployees() {
        try {
            String[] info = Files.readAllLines(Paths.get("rooms\\res\\password.txt")).toArray(new String[0]);

            connection = DriverManager.getConnection("jdbc:mysql://" + info[0] + ":3306/HotelManagement", info[1], info[2]);

            PreparedStatement chefStatement = connection.prepareStatement("SELECT * FROM chef;");
            ResultSet chefSet = chefStatement.executeQuery();
            while (chefSet.next()) {
                chefs.add(new Chef(chefSet.getInt(1), chefSet.getInt(2),
                        chefSet.getString(3), chefSet.getString(4), chefSet.getString(5)));
            }
            PreparedStatement managerStatement = connection.prepareStatement("SELECT * FROM manager;");
            ResultSet managerSet = managerStatement.executeQuery();
            while (managerSet.next()) {
                managers.add(new Manager(managerSet.getInt(1), managerSet.getInt(2),
                        managerSet.getString(3), managerSet.getString(4), managerSet.getString(5)));
            }
            PreparedStatement washerStatement = connection.prepareStatement("SELECT * FROM washer;");
            ResultSet washerSet = washerStatement.executeQuery();
            while (washerSet.next()) {
                washers.add(new Washer(washerSet.getInt(1), washerSet.getInt(2),
                        washerSet.getString(3), washerSet.getString(4), washerSet.getString(5)));
            }


        } catch (SQLException | IOException e) {
            e.printStackTrace();
        }
    }


    public Chef addChef(int age, String firstName, String lastName, String email) {
        try {
            PreparedStatement statement = connection.prepareStatement("INSERT INTO chef VALUES (DEFAULT, ?, ?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
            statement.setInt(1, age);
            statement.setString(2, firstName);
            statement.setString(3, lastName);
            statement.setString(4, email);
            statement.setDouble(5, 10);
            statement.executeUpdate();

            ResultSet generatedKeys = statement.getGeneratedKeys();
            Chef chef = new Chef(Utilities.getRowOneColumnOne(generatedKeys), age, firstName, lastName, email);
            chefs.add(chef);
            return chef;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return new Chef(0, 0, null, null, null);
    }

    public Manager addManager(int age, String firstName, String lastName, String email) {
        try {
            PreparedStatement statement = connection.prepareStatement("INSERT INTO manager VALUES (DEFAULT, ?, ?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
            statement.setInt(1, age);
            statement.setString(2, firstName);
            statement.setString(3, lastName);
            statement.setString(4, email);
            statement.setDouble(5, 15);
            statement.executeUpdate();

            ResultSet generatedKeys = statement.getGeneratedKeys();
            Manager manager = new Manager(Utilities.getRowOneColumnOne(generatedKeys), age, firstName, lastName, email);
            managers.add(manager);
            return manager;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return new Manager(0, 0, null, null, null);
    }

    public Washer addWasher(int age, String firstName, String lastName, String email) {
        try {
            PreparedStatement statement = connection.prepareStatement("INSERT INTO washer VALUES (DEFAULT, ?, ?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
            statement.setInt(1, age);
            statement.setString(2, firstName);
            statement.setString(3, lastName);
            statement.setString(4, email);
            statement.setDouble(5, 15);
            statement.executeUpdate();

            ResultSet generatedKeys = statement.getGeneratedKeys();
            Washer washer = new Washer(Utilities.getRowOneColumnOne(generatedKeys), age, firstName, lastName, email);
            washers.add(washer);
            return washer;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return new Washer(0, 0, null, null, null);
    }

    public void save() {
        chefs.stream().filter(BaseEmployee::hasChanged).forEach(chef -> Utilities.updateChef(chef, connection));
        washers.stream().filter(BaseEmployee::hasChanged).forEach(washer -> Utilities.updateWasher(washer, connection));
        managers.stream().filter(BaseEmployee::hasChanged).forEach(manager -> Utilities.updateManager(manager, connection));
    }


    public void removeChef(int id) {
        try {
            PreparedStatement statement = connection.prepareStatement("DELETE FROM chef WHERE id=?");
            statement.setInt(1, id);
            statement.executeUpdate();

            chefs.removeIf(chef -> chef.getId() == id);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void removeManager(int id) {
        try {
            PreparedStatement statement = connection.prepareStatement("DELETE FROM manager WHERE id=?");
            statement.setInt(1, id);
            statement.executeUpdate();

            managers.removeIf(manager -> manager.getId() == id);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void removeWasher(int id) {
        try {
            PreparedStatement statement = connection.prepareStatement("DELETE FROM washer WHERE id=?");
            statement.setInt(1, id);
            statement.executeUpdate();

            washers.removeIf(washer -> washer.getId() == id);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public ArrayList<Manager> getManagers() {
        return managers;
    }

    public ArrayList<Washer> getWashers() {
        return washers;
    }

    public ArrayList<Chef> getChefs(){
        return chefs;
    }
}
