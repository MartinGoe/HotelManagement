package de.cube.hotel.rooms.classes;

import de.cube.hotel.rooms.functions.Utilities;

import java.util.Date;

public class SingleRoom extends BaseRoom {
    public SingleRoom(int id, int roomNumber, int isUsed, java.sql.Date lastUsedDay) {
        super(id, roomNumber, isUsed, lastUsedDay, 100);

    }
}
