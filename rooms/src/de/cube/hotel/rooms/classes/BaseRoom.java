package de.cube.hotel.rooms.classes;

import java.util.Date;

public class BaseRoom {


    private int id;

    private int costPerDay;
    private int roomNumber;

    private int isUsed;
    private boolean isUsedBool;

    private java.sql.Date lastUsedDay;

    private boolean hasChanged = false;


    BaseRoom(int id, int roomNumber, int isUsed, java.sql.Date lastUsedDay, int costPerDay) {
        this.id = id;
        this.costPerDay = costPerDay;
        this.roomNumber = roomNumber;
        this.isUsed = isUsed;
        this.lastUsedDay = lastUsedDay;
    }

    public int getId() {
        return id;
    }

    public int getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(int roomNumber) {
        this.roomNumber = roomNumber;
        hasChanged = true;
    }

    public int isUsed() {
        return isUsed;
    }

    public void setUsed(int used) {
        isUsed = used;
        if(isUsed == 1)
            isUsedBool = true;
        else
            isUsedBool = false;
        hasChanged = true;
    }

    public java.sql.Date getLastUsedDay() {
        return lastUsedDay;
    }

    public void setLastUsedDay(java.sql.Date lastUsedDay) {
        this.lastUsedDay = lastUsedDay;
        hasChanged = true;
    }

    public int getCostPerDay() {
        return costPerDay;
    }

    public void setCostPerDay(int costPerDay) {
        this.costPerDay = costPerDay;
        hasChanged = true;
    }

    public boolean hasChanged () {return hasChanged;}

    public boolean isUsedBool() {
        return isUsedBool;
    }
}
