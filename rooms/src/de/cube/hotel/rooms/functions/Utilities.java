package de.cube.hotel.rooms.functions;

import de.cube.hotel.rooms.classes.DoubleRoom;
import de.cube.hotel.rooms.classes.SingleRoom;
import de.cube.hotel.rooms.classes.Suite;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Utilities {

    static int getRowOneColumnOne(ResultSet rs){
        try {
            rs.next();
            return rs.getInt(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }
     static void updateSingleRoom(SingleRoom singleRoom, Connection connection) {
        try {
            PreparedStatement statement = connection.prepareStatement("UPDATE singleroom SET RoomNumber=?, isUsed=?, lastUsedDay=?, CostPerDay=? WHERE id=?;");
            statement.setInt(1, singleRoom.getRoomNumber());
            statement.setInt(2, singleRoom.isUsed());
            statement.setDate(3, singleRoom.getLastUsedDay());
            statement.setInt(4, singleRoom.getCostPerDay());
            statement.setInt(5, singleRoom.getId());
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    static void updateDoubleRoom(DoubleRoom doubleRoom, Connection connection) {
        try {
            PreparedStatement statement = connection.prepareStatement("UPDATE doubleroom SET RoomNumber=?, isUsed=?, lastUsedDay=?, CostPerDay=? WHERE id=?;");
            statement.setInt(1, doubleRoom.getRoomNumber());
            statement.setInt(2, doubleRoom.isUsed());
            statement.setDate(3, doubleRoom.getLastUsedDay());
            statement.setInt(4, doubleRoom.getCostPerDay());
            statement.setInt(5, doubleRoom.getId());
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    static void updateSuite(Suite suite, Connection connection) {
        try {
            PreparedStatement statement = connection.prepareStatement("UPDATE suite SET roomnumber=?, isUsed=?, lastUsedDay=?, CostPerDay=? WHERE id=?;");
            statement.setInt(1, suite.getRoomNumber());
            statement.setInt(2, suite.isUsed());
            statement.setDate(3, suite.getLastUsedDay());
            statement.setInt(4, suite.getCostPerDay());
            statement.setInt(5, suite.getId());
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


}
