package de.cube.hotel.rooms.functions;

import de.cube.hotel.rooms.classes.BaseRoom;
import de.cube.hotel.rooms.classes.DoubleRoom;
import de.cube.hotel.rooms.classes.SingleRoom;
import de.cube.hotel.rooms.classes.Suite;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.*;
import java.util.ArrayList;

public class ManageRooms {

    Connection connection;


    ArrayList<Suite> suites = new ArrayList<>();
    ArrayList<DoubleRoom> doubleRooms = new ArrayList<>();
    ArrayList<SingleRoom> singleRooms = new ArrayList<>();

    public ManageRooms() {
        try {
            String[] info = Files.readAllLines(Paths.get("rooms\\res\\password.txt")).toArray(new String[0]);

            connection = DriverManager.getConnection("jdbc:mysql://" + info[0] + ":3306/HotelManagement", info[1], info[2]);

            PreparedStatement singleRoomStatement = connection.prepareStatement("SELECT * FROM singleroom");
            ResultSet singleRoomSet = singleRoomStatement.executeQuery();
            while (singleRoomSet.next()) {
                singleRooms.add(new SingleRoom(singleRoomSet.getInt(1), singleRoomSet.getInt(2),
                        singleRoomSet.getInt(3), singleRoomSet.getDate(4)));
            }

            PreparedStatement doubleRoomStatement = connection.prepareStatement("SELECT * FROM doubleroom");
            ResultSet doubleRoomSet = doubleRoomStatement.executeQuery();
            while (doubleRoomSet.next()) {
                doubleRooms.add(new DoubleRoom(doubleRoomSet.getInt(1), doubleRoomSet.getInt(2),
                        doubleRoomSet.getInt(3), doubleRoomSet.getDate(4)));
            }

            PreparedStatement suiteStatement = connection.prepareStatement("SELECT * FROM singleroom");
            ResultSet suiteSet = suiteStatement.executeQuery();
            while (suiteSet.next()) {
                suites.add(new Suite(suiteSet.getInt(1), suiteSet.getInt(2),
                        suiteSet.getInt(3), suiteSet.getDate(4)));
            }
        } catch (SQLException | IOException e) {
            e.printStackTrace();
        }
    }



    public void addSingleRoom(int roomNumber, int isUsed, Date lastUsedDay) {
        try {
            PreparedStatement statement = connection.prepareStatement("INSERT INTO singleroom VALUES (DEFAULT, ?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
            statement.setInt(1, roomNumber);
            statement.setInt(2, isUsed);
            statement.setDate(3, lastUsedDay);
            statement.setInt(4, 100);
            statement.executeUpdate();

            ResultSet generatedKeys = statement.getGeneratedKeys();
            SingleRoom singleRoom = new SingleRoom(Utilities.getRowOneColumnOne(generatedKeys), roomNumber, isUsed, lastUsedDay);
            singleRooms.add(singleRoom);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void addDoubleRoom(int roomNumber, int isUsed, java.sql.Date lastUsedDay) {
        try {
            PreparedStatement statement = connection.prepareStatement("INSERT INTO doubleroom VALUES (DEFAULT, ?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
            statement.setInt(1, roomNumber);
            statement.setInt(2, isUsed);
            statement.setDate(3, lastUsedDay);
            statement.setInt(4, 150);
            statement.executeUpdate();

            ResultSet generatedKeys = statement.getGeneratedKeys();

            doubleRooms.add(new DoubleRoom(Utilities.getRowOneColumnOne(generatedKeys), roomNumber, isUsed, lastUsedDay));

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<Suite> getSuites() {
        return suites;
    }

    public ArrayList<DoubleRoom> getDoubleRooms() {
        return doubleRooms;
    }

    public ArrayList<SingleRoom> getSingleRooms() {
        return singleRooms;
    }

    public void addSuite(int roomNumber, int isUsed, java.sql.Date lastUsedDay) {
        try {
            PreparedStatement statement = connection.prepareStatement("INSERT INTO suite VALUES (DEFAULT, ?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
            statement.setInt(1, roomNumber);
            statement.setInt(2, isUsed);
            statement.setDate(3, lastUsedDay);
            statement.setInt(4, 200);
            statement.executeUpdate();

            ResultSet generatedKeys = statement.getGeneratedKeys();

            suites.add(new Suite(Utilities.getRowOneColumnOne(generatedKeys), roomNumber, isUsed, lastUsedDay));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void save() {
        singleRooms.stream().filter(BaseRoom::hasChanged).forEach(singleRoom -> Utilities.updateSingleRoom(singleRoom, connection));
        doubleRooms.stream().filter(BaseRoom::hasChanged).forEach(doubleRoom -> Utilities.updateDoubleRoom(doubleRoom, connection));
        suites.stream().filter(BaseRoom::hasChanged).forEach(suite -> Utilities.updateSuite(suite, connection));
    }


    //TODO
    public void removeSuite(int id){
        try {
            PreparedStatement statement = connection.prepareStatement("DELETE FROM suites WHERE id=?");
            statement.setInt(1, id);
            statement.executeUpdate();

            suites.removeIf(suite -> suite.getId() == id);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void removeSingleRoom(int id){
        try {
            PreparedStatement statement = connection.prepareStatement("DELETE FROM singleroom WHERE id=?");
            statement.setInt(1, id);
            statement.executeUpdate();

            singleRooms.removeIf(singleRoom -> singleRoom.getId() == id);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void removeDoubleRoom(int id){
        try {
            PreparedStatement statement = connection.prepareStatement("DELETE FROM doubleroom WHERE id=?");
            statement.setInt(1, id);
            statement.executeUpdate();

            doubleRooms.removeIf(doubleRoom -> doubleRoom.getId() == id);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


}
